import { axiosInstance } from 'boot/axios'

export async function getAll() {
  return await axiosInstance.get('/role')
}
