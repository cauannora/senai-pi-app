import { Cookies } from 'quasar'
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import atividade from './atividade'
import atividadePendente from './atividade-pendente'
import auth from './auth'
import role from './role'
import usuario from './usuario'

Vue.use(Vuex)

const vuexCookie = new VuexPersistence({
  restoreState: (key) => Cookies.get(key),
  saveState: (key, state) =>
    Cookies.set(key, state, {
      path: '/',
      expires: Number(process.env.COOKIE_EXPIRE),
      secure: process.env.COOKIE_SECURE === 'true'
    }),
  modules: ['auth']
})

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      role,
      usuario,
      atividade,
      atividadePendente
    },

    plugins: [vuexCookie.plugin]
    // enable strict mode (adds overhead!)
    // for dev mode only
    // strict: process.env.DEBUGGING
  })

  return Store
}
