export default function () {
  return {
    title: {
      singular: 'Atividade Pendente',
      plural: 'Atividades Pendentes'
    },
    columns: [
      {
        name: 'id',
        label: '#',
        align: 'left',
        field: 'id',
        sortable: true
      },
      {
        name: 'carga_horaria',
        label: 'Carga Horaria',
        align: 'left',
        field: 'carga_horaria',
        sortable: true,
        form: {
          type: 'text',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 40
        }
      },
      {
        name: 'documentacao',
        label: 'Exige Documentação?',
        align: 'left',
        field: 'documentacao',
        sortable: true,
        form: {
          type: 'toggle',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 40
        }
      },
      {
        name: 'observacao',
        label: 'Observação',
        align: 'left',
        field: 'observacao',
        sortable: true,
        form: {
          type: 'textarea',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 180
        }
      },
      {
        name: 'status',
        label: 'Status',
        align: 'left',
        field: 'status',
        sortable: true,
        form: {
          type: 'text',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 40
        }
      },
      {
        name: 'usuario_id',
        label: 'Aluno',
        align: 'left',
        field: 'usuario_id',
        sortable: true,
        form: {
          type: 'text',
          ref: 'usuario/getOne',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 40
        }
      },
      {
        name: 'url_documentacao',
        label: 'URL Documentação',
        align: 'left',
        field: 'url_documentacao',
        sortable: true,
        hideColumn: true,
        form: {
          type: 'text',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 40
        }
      }
    ]
  }
}
