import { axiosInstance } from 'boot/axios'

export async function getAll(_, params) {
  return await axiosInstance.get('/activity', { params })
}

export async function getOne(_, id) {
  return await axiosInstance.get(`/activity/${id}`)
}

export async function check(_, params) {
  return await axiosInstance.get('/activity/check', { params })
}

export async function create(_, props) {
  return await axiosInstance.post('/activity/', props.data)
}

export async function update(_, props) {
  props.data.id = props.id
  return await axiosInstance.put(`/activity/${props.id}`, props.data)
}

export async function remove(_, id) {
  return await axiosInstance.delete(`/activity/${id}`)
}
