import { axiosInstance } from 'boot/axios'
import { Cookies } from 'quasar'

export async function login({ commit }, auth) {
  try {
    const { data } = await axiosInstance.post('/auth/login', auth)
    commit('setToken', data.token)
    commit('setUser', data.user)
    return data
  } catch (error) {
    throw new Error(error)
  }
}

export function logout({ commit }) {
  commit('logout')
  Cookies.remove('vuex', { path: '/' })
}

export async function refreshUser({ commit, state }, user) {
  if (state.user.id === user.id) {
    const { data } = await axiosInstance.get(`/user/${user.id}`)
    commit('setUser', data)
  }
}
