export function setToken(state, token) {
  state.token = token
}

export function setUser(state, user) {
  state.user = user
}

export function logout(state) {
  state.user = ''
  state.token = ''
}
