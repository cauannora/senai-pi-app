export function getRaw(state) {
  const raw = []
  raw.title = state.title
  raw.columns = state.columns
  return raw
}
