import { cpf } from 'cpf-cnpj-validator'

export default function() {
  return {
    title: {
      singular: 'Usuário',
      plural: 'Usuários'
    },
    columns: [
      {
        name: 'id',
        label: '#',
        align: 'left',
        field: 'id',
        sortable: true
      },
      {
        name: 'nome',
        label: 'Nome',
        align: 'left',
        field: 'nome',
        sortable: true,
        form: {
          type: 'text',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          maxlength: 40
        }
      },
      {
        name: 'email',
        label: 'E-mail',
        align: 'left',
        field: 'email',
        sortable: true,
        form: {
          type: 'email',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true,
          unique: true,
          maxlength: 50
        }
      },
      {
        name: 'documento',
        label: 'CPF',
        align: 'left',
        field: 'documento',
        format: (val) => cpf.format(val),
        sortable: true,
        form: {
          type: 'cpf',
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true
        }
      },
      {
        name: 'role',
        label: 'Tipo',
        align: 'left',
        field: (row) => row.role,
        format: (val) => val.descricao,
        sortable: true,
        form: {
          type: 'select',
          ref: 'role/getAll',
          options: {
            label: 'name'
          },
          class: {
            parent: 'col-xs-12 col-md-6'
          },
          required: true
        }
      },
      {
        name: 'password',
        label: 'Senha',
        align: 'left',
        field: 'password',
        sortable: true,
        hideColumn: true,
        form: {
          type: 'password',
          class: {
            parent: 'col-xs-12 col-md-6'
          }
        }
      }
    ]
  }
}
