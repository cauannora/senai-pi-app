export function truncate(text, length) {
  const words = text.split(' ')
  if (words.length <= length) {
    return text
  } else {
    return words.splice(0, length).join(' ') + ' ...'
  }
}
export function strPad(number, size) {
  let strNumber = `${number}`
  while (strNumber.length < size) {
    strNumber = '0' + strNumber
  }
  return strNumber
}
