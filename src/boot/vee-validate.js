import { cpf } from 'cpf-cnpj-validator'
import moment from 'moment'
import { extend } from 'vee-validate'
import { email, required } from 'vee-validate/dist/rules'

export default ({ store }) => {
  async function isUnique(props, value) {
    const params = {}

    if (props[3] === 'cpf') {
      value = cpf.strip(value)
    }

    params[props[1]] = value

    if (props[2] !== 'null') params.id = props[2]

    const { data } = await store.dispatch(`${props[0]}/check`, params)
    return !data.unique
  }
  extend('required', {
    ...required,
    message: 'Não pode estar vazio'
  })

  extend('email', {
    ...email,
    message: 'Deve ser um e-mail válido'
  })

  extend('date', {
    validate: (value) => {
      return moment(value, 'DD/MM/YYYY', true).isValid()
    },
    message: 'Deve ser uma data válida'
  })

  extend('datetime', {
    validate: (value) => {
      return moment(value, 'DD/MM/YYYY HH:mm', true).isValid()
    },
    message: 'Deve ser uma data e um horário válidos'
  })

  extend('time', {
    validate: (value) => {
      return moment(value, 'HH:mm', true).isValid()
    },
    message: 'Deve ser um horário válido'
  })

  extend('cpf', {
    validate: (value) => {
      return cpf.isValid(value)
    },
    message: 'Deve ser um CPF válido'
  })

  extend('unique', {
    validate: (value, props) => {
      return isUnique(props, value)
    },
    message: 'Já está sendo utilizado'
  })

  extend('password', {
    validate: (value) => {
      const strongRegex = new RegExp(
        '^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})'
      )
      return strongRegex.test(value)
    },
    message: 'Deve ter no mínimo 8 caracteres com letras, números e símbolos'
  })

  extend('confirmationPassword', {
    validate: (value, props) => {
      return value === props[0]
    },
    message: 'As senhas estão diferentes'
  })
}
