function authorized(to, raw) {
  return !!to.meta.roles.includes(raw.user.role.id)
}

export default ({ router, store }) => {
  router.beforeEach((to, _from, next) => {
    const raw = store.getters['auth/getRaw']
    if (to.matched.some((record) => record.meta.requiresAuth)) {
      if (raw.token) {
        store.dispatch('auth/refreshUser', raw.user).then()
        if (authorized(to, raw)) {
          next()
        } else {
          next('/')
        }
      } else {
        next({
          path: '/login',
          params: { redirect: to.fullPath }
        })
      }
    } else if (to.matched.some((record) => record.meta.guest)) {
      if (raw.token) {
        next('/')
      } else {
        next()
      }
    } else {
      next()
    }
  })
}
