import axios from 'axios'
import Vue from 'vue'

const axiosInstance = axios.create({
  baseURL: process.env.API_URL,
  withCredentials: true
})

export default ({ store }) => {
  axiosInstance.interceptors.request.use((request) => {
    const authToken = store.getters['auth/getRaw'].token

    if (authToken) {
      request.headers.Authorization = `Bearer ${authToken}`
    }

    return request
  })
}

Vue.prototype.$http = axiosInstance
export { axiosInstance }
