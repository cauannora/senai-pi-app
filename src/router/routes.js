const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout'),
    children: [
      {
        path: '',
        component: () => import('../pages/Dashboard'),
        meta: { roles: [1, 2, 3, 4, 5] }
      },
      {
        path: 'usuarios',
        component: () => import('../pages/Usuarios'),
        meta: { roles: [1, 2, 3] }
      },
      {
        path: 'atividades',
        component: () => import('../pages/Atividades'),
        meta: { roles: [1, 2, 3, 4, 5] }
      },
      {
        path: 'atividades-pendentes',
        component: () => import('../pages/AtividadesPendentes'),
        meta: { roles: [1, 2, 3, 4] }
      }
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/login',
    component: () => import('layouts/LoginLayout'),
    meta: {
      guest: true
    }
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
